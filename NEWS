SubLib
======

0.9 (10/05/2008)
================

- Added support for the following subtitle formats:
  * AQ Title
  * MacSUB
  * Sofni
  * SubCreator 1.x
  * ViPlay Subtitle File
- Improved subtitle format auto-detection
- Allow to install the library with autotools


0.8 (06/10/2007)
================

- Added support for the following subtitle formats:
  * Adobe Encore DVD
  * DKS Subtitle Format
  * Karaoke Lyrics LRC
  * Karaoke Lyrics VKT
  * Panimator
  * Phoenix Japanimation Society
  * Power DivX
- Different subtitle formats share similar headers, when edited
- Allow to import text lines, without timings, from a plain text file


0.7 (28/06/2007)
================

- Added support for subtitle translation
  * Translation property in each Subtitle
  * Search methods now search in translation too
  * Select the type of text content (text or translation) to use when saving
  * Allow to clear translations and import a translation into existing subtitles
- Try every detected encoding instead of only the first one, and use the first
  encoding that works
- Allow to trim subtitle lines when getting them
- When replacing blank lines, also apply the replacement if the text is empty
- EncodingNotSupportedException is now thrown if an Encoding is not supported by
  the platform, when opening a file
- Fixed some bugs that were visible when doing a backwards search
- Updated NCharDet


0.6.1 (12/05/2007)
==================

- Added a property to specify the type of newline (Macintosh, Unix or Windows)
  to be used when saving Subtitles


0.6 (13/04/2007)
================

- File-related state (character coding, path, subtitle format) isn't
  maintained anymore after parsing
- SubtitleProperties now only includes properties that aren't file-related
- FileProperties includes all the file-related properties
- Refactored the parsing engine, with ParsingProperties being used to store the
  properties set during parsing
- Fixed the codepage attribution for some character codings, with some being
  removed as no codepages were available
- SubtitleSaver now includes all file saving functionality, which was
  simplified with most overloaded methods being removed
- New and Open methods were also simplified
- Updated the documentation


0.5 (05/03/2007)
================

- Find a subtitle given a specific time position
- Convert from time to frames given the time in seconds


0.4 (08/12/2006)
================
- Added Search functionality:
  * Find with regular expressions
  * Find next or previous
  * Find with specified case sensitivity and wrapping options
  * Start search on the specified subtitle and text index
- Added ReplaceAll functionality
- Now updates the timing mode after saving a file with a different timing mode
- Moved to the C# 2.0 compiler


0.3 (30/10/2006)
================

- Encoding auto-detection can now fallback to a specified encoding (and possibly
  try to detect a Unicode BOM) when auto-detection fails
- Encoding auto-detection throws UnknownEncodingException when encoding is not
  detected
- Renamed UnknownCharEncodingException to UnknownEncodingException
- Added property to get the preferred extension for a SubtitleType
- Methods and properties for returning info on the available subtitle types are
  now static
- The Subtitles.AvailableTypes property doesn't sort types from now on, the new
  AvailableTypesSorted property does sort the types
- Added method to return a SubtitleTypeInfo given a SubtitleType
- Open files are now guaranteed to be closed when errors occur
- Added method to search for an extension in a SubtitleTypeInfo


0.2 (04/10/2006)
================

- Added support for the following subtitle formats:
  * SubViewer 1.0
  * SubViewer 2.0
  * MPlayer
  * MPlayer 2
  * MPSub (both time and frame modes)
  * Sub Station Alpha
  * Advanced Sub Station Alpha
- Auto-adjust timings based on 2 correct times / synchronization points, can be
  used to correct all subtitles or just a range
- Shift the whole set of subtitles or just a range (previously each subtitle
  could be set individually)
- Added support for headers, which are accessed in SubtitleProperties
  individually for each subtitle format that supports them
- Improved the performance of the parsing engine


0.1 (30/08/2006)
================

- Store subtitle-related properties (eg. SubtitleType, TimingMode, Encoding)
  after reading a file
- New methods for Save, Save As, and to know if a file can be saved or requires
  "saving as"
- Heuristics change in subtitle format detection, a smaller part of the text is
  used and a rigid match can return immediately
- Regular expressions are not compiled anymore and were simplified to improve
  readability
- OutputExpression uses the system's newline (\r\n on windows, \n on unix)
- Included descriptions for the subtitle types, such as their extensions and
  their names
- Text is trimmed by default when opening subtitles, but not trimmed when using
  Set with only 1 argument (the text)
- Internal code cleanup and refactoring
- Added methods to create and insert new subtitles at a specified position, or
  after/before existing subtitles, basing the new times on the existing
  subtitles
- Current and Original frame rates can only be set internally, so they cannot be
  changed without using Subtitles.ChangeFrameRate or
  Subtitles.ChangeOriginalFrameRate
- Corrected bug with not reading the last character when parsing input in some
  occasions
- Added support for shifting subtitles' timings
- Now compiles with .Net 1.1
- Added support for autotools and MonoDevelop solution files
- Written complete documentation


version 0.0.5 (27/02/2006)
==========================

- Major namespace change, everything is inside the "SubLib" namespace now
- Internal file organization change, Domain files are now under Application
- Now tries western 1252 encoding if both auto-detection and the default
  encoding fail
- Stop throwing exceptions when times and frames are accessed and invalid (use
  IsValid to validate)
- Possibility to create a new/empty subtitle file
- Associate a new subtitle file to a filename or set it unnamed
- SubtitleProperties now handles file path and allows operations on it
- Subtitle's duration field can now be changed, and not only the end field
- Subtitles are now indexed starting at 0, instead of 1
- Enumerations changed, removed the "Unknown" item for consistency
- New constructors to Times, Frames, Subtitle, SubtitleText and Subtitles
- Code cleanup

Bugs fixed
- Adding a subtitle to the beginning of the collection inserts in position 1
  instead of 0


version 0.0.4 (29/01/2006)
==========================

- MicroDVD: now detects style tags anywhere inside subtitles' text
- SubRip: fixed bug related to style parsing
- Parsing engine optimized 
- Subtitle regular expressions are case-insensitive by default
- Subtitles are now trimmed
- Added Get() to SubtitleText, which retrieves subtitles separated by the
  newline character


version 0.0.3 (10/29/2005)
==========================

- Architecture restructured, now features a rich domain model which interacts
  with both the application and persistency layers
- Subtitle domain now includes frames and times as objects
- Frames and Times automatically update each other when changed
- Parsing engine rewrite
- Improved parsing engine's efficiency, using a string builder instead of common
  strings
- Added an optional argument to the testing executable, to change the input fps
  before opening subtitles
- Updated the nant build file with targets to make releases
- Updated the README file with the most current testing executable usage


version 0.0.2 (10/23/2005)
==========================

- Added a Post Process mechanism to the parsing engine
- Fixed some bugs with reading style tags from SubRip subtitles
- Has the possibility of displaying more verbose prints
- Properties mechanism sets the way subtitles are opened
- New operations: changing the framerate and the input file's framerate
- Includes a README file


version 0.0.1 (09/24/2005)
==========================

- Subtitle format auto detection
- Support for MicroDVD and SubRip subtitles
- Auto detect the file's character encoding (using the Mozilla libraries)
- Generic parsing engine allows to add new subtitle formats using regular
  expressions
- Relaxed rules for parsing subtitles with errors
- Lists the bits of text which couldn't be read
- Supported operations: reading and writing

