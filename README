SubLib
====================
http://sublib.sf.net

SubLib is a library that eases the development of subtitling applications. It
supports the most common text-based subtitle formats and allows for subtitle
editing, conversion and synchronization.

SubLib is written in C# and can be used in platforms like Mono or .NET Framework.


Building from Source
====================

Windows users can use NAnt (3) or import the project into Visual Studio.
Linux users can use any of the following three.

1. Autotools
	* Build it with the usual `./configure' followed by `make' commands.
	* Install it with `make install'.
    * See the INSTALL file for more information

2. MonoDevelop
    * Use the `sublib.mds' file to open the project with MonoDevelop

3. NAnt
    * Use `nant build' to build the library as a binary DLL
    * Use `nant build-as-exe' to build the library as an EXE that allows to test
	  part of its features
    * Use `nant build-docs' to build the docs


Building from SVN
=================

Checkout the latest code from the SVN:
	svn co https://sublib.svn.sf.net/svnroot/sublib/trunk/sublib

Run `./autogen.sh' to generate the build scripts. To build SubLib, use the
instructions in "Building from Source".
